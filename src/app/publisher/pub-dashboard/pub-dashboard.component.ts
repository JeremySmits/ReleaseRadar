import { Component, OnInit } from '@angular/core';
import { PublisherService } from '../../Services/publisher.service'
import { UserService } from 'src/app/Services/user.service'

@Component({
  selector: 'app-pub-dashboard',
  templateUrl: './pub-dashboard.component.html',
  styleUrls: ['./pub-dashboard.component.scss']
})
export class PubDashboardComponent implements OnInit {
  publishers = []
  constructor(private publisherService: PublisherService, public userService: UserService) {}

  ngOnInit() {
    this.loadPublishers()
  }

  loadPublishers() {
    this.publisherService.getPublishers().subscribe(data => (this.publishers = data))
  }
}
