import { Component, OnInit } from '@angular/core'
import { Publisher } from '../../Models/publisher'
import {PublisherService} from '../../Services/publisher.service'
import { ActivatedRoute } from '@angular/router'
import { Router} from '@angular/router'

@Component({
  selector: 'app-updatepublisher',
  templateUrl: './updatepublisher.component.html',
  styleUrls: ['./updatepublisher.component.scss']
})
export class UpdatepublisherComponent implements OnInit {
  publisherModel = new Publisher('','','','','','',localStorage.getItem('userId'));
  errorMsg ='';
  publisherId = this.route.snapshot.paramMap.get('id')

  constructor(private publisherService: PublisherService, private route: ActivatedRoute, private router: Router) {}
  ngOnInit( ) { this.loadPublisher()}
  onSubmit() {
    this.publisherService.putPublisher(this.publisherModel, this.publisherId )
    .subscribe(
      data => this.router.navigate(['../publishers']),
      error => this.errorMsg = error.statusText
    )
  }
  loadPublisher() {
    this.publisherService.getPublisherById(this.publisherId).subscribe(data => {this.publisherModel = data
      if(this.publisherModel.user != localStorage.getItem('userId'))
      {
        this.router.navigate(['../dashboard'])
      }
    })
  }
}
