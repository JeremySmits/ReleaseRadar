import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { PublisherService } from '../../Services/publisher.service'
import { DomSanitizer } from '@angular/platform-browser'
import { MatDialog} from '@angular/material'
import { DeletedialogComponent } from '../../core/deletedialog/deletedialog.component'
import {Router} from '@angular/router'
import { UserService } from '../../Services/user.service'

@Component({
  selector: 'app-detailedpublisher',
  templateUrl: './detailedpublisher.component.html',
  styleUrls: ['./detailedpublisher.component.scss']
})
export class DetailedpublisherComponent implements OnInit {
  publisher = null
  owner = false;
  true = true;
  constructor(
    private route: ActivatedRoute,
    private publisherService: PublisherService,
    private sanitizer: DomSanitizer,
    private dialog: MatDialog,
    private router: Router,
    public userService: UserService
  ) {}

  ngOnInit() {
    this.loadPublisher()
  }
  loadPublisher() {
    const publisherId = this.route.snapshot.paramMap.get('id')
    this.publisherService.getPublisherById(publisherId).subscribe(data => {this.publisher = data
      if(this.publisher.user === localStorage.getItem('userId'))
      {
        this.owner = true;
      }
    })
  }
  openDeleteDialog()
  {
    let dialogRef = this.dialog.open(DeletedialogComponent);
    dialogRef.afterClosed().subscribe(result => {
        if(result == "true")
        {
          const publisherId = this.route.snapshot.paramMap.get('id')
          this.publisherService.deleltePublisherById(publisherId )
          .subscribe(
            data => this.router.navigate(['../publishers']),
            error => console.log(error.statusText)
          )
        }
    });
  }
}
