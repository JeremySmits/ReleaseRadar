import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { DetailedpublisherComponent } from './detailedpublisher.component'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { MatDialogModule } from '@angular/material'

describe('DetailedpublisherComponent', () => {
  let component: DetailedpublisherComponent
  let fixture: ComponentFixture<DetailedpublisherComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule,HttpClientTestingModule, MatDialogModule],
      declarations: [DetailedpublisherComponent]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailedpublisherComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
