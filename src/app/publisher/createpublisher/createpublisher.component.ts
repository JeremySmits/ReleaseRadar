import { Component, OnInit } from '@angular/core'
import { Publisher } from '../../Models/publisher'
import {PublisherService} from '../../Services/publisher.service'
import { UserService } from '../../Services/user.service';

@Component({
  selector: 'app-createpublisher',
  templateUrl: './createpublisher.component.html',
  styleUrls: ['./createpublisher.component.scss']
})
export class CreatepublisherComponent implements OnInit {
  publisherModel = new Publisher('','','','','','',localStorage.getItem('userId'));
  errorMsg ='';
  constructor(private publisherService: PublisherService, public userService: UserService) {}
  ngOnInit() {}
  onSubmit() {
    this.publisherService.postPublisher(this.publisherModel)
    .subscribe(
      data => window.location.href = '../publishers',
      error => this.errorMsg = error.statusText
    )
  }
}
