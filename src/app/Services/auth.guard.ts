import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router } from '@angular/router';
import { UserService} from './user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate{

  constructor(public userService: UserService,
    private router:Router){}

  canActivate(): boolean{
    if(this.userService.getToken()){
      return true
    }
    else{
      this.router.navigate(['/login'])
      return false
    }
    
  }
  getLocalStorage(): string{
    return localStorage.getItem('userId')
  }
}
