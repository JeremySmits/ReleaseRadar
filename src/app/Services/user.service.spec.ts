import { TestBed, inject } from '@angular/core/testing';

import { UserService } from './user.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

describe('UserService', () => {
  beforeEach(() => TestBed.configureTestingModule({imports: [HttpClientTestingModule,RouterTestingModule],}));

  it('should be created', () => {
    const service: UserService = TestBed.get(UserService);
    expect(service).toBeTruthy();
  });
  
  it('should have getToken function',
  inject([UserService], (service: UserService) => {
    expect(service.getToken).toBeTruthy();
  }));

  it('should have getUsers function',
  inject([UserService], (service: UserService) => {
    expect(service.getUsers).toBeTruthy();
  }));

  it('should have loggedIn function',
  inject([UserService], (service: UserService) => {
    expect(service.loggedIn).toBeTruthy();
  }));

  it('should have loginUser function',
  inject([UserService], (service: UserService) => {
    expect(service.loginUser).toBeTruthy();
  }));

  it('should have logoutUser function',
  inject([UserService], (service: UserService) => {
    expect(service.logoutUser).toBeTruthy();
  }));

  it('should have postUser function',
  inject([UserService], (service: UserService) => {
    expect(service.postUser).toBeTruthy();
  }));

  it('should retrueve users from API',
  inject([UserService], (service: UserService) => {
    expect(service.getUsers().subscribe(data => {
      expect(data.length).toBeGreaterThan(0);
    }));
  }));
});
 