import { Injectable } from '@angular/core'
import { of, Observable } from 'rxjs'
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Publisher } from '../Models/publisher'
import {catchError} from 'rxjs/operators'
import {throwError} from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class PublisherService {
  constructor(private http: HttpClient) {}

  getPublishers(): Observable<any> {
    return this.http.get('https://releaseradarapi.herokuapp.com/api/publishers')
  }
  getPublisherById(publisherId): Observable<any> {
    return this.http.get('https://releaseradarapi.herokuapp.com/api/publishers/' + publisherId)
  }
  deleltePublisherById(publisherId): Observable<any> {
    return this.http.delete('https://releaseradarapi.herokuapp.com/api/publishers/' + publisherId)
  }
  postPublisher(publisher: Publisher): Observable<any> {
    return this.http.post<any>('https://releaseradarapi.herokuapp.com/api/publishers/', publisher)
      .pipe(catchError(this.errorHandler))
  }
  putPublisher(publisher: Publisher, publisherId): Observable<any> {
    return this.http.put<any>('https://releaseradarapi.herokuapp.com/api/publishers/'+ publisherId, publisher)
      .pipe(catchError(this.errorHandler))
  }
  errorHandler(error: HttpErrorResponse){
    return throwError(error);
  }
}
