import { Injectable } from '@angular/core'
import { of, Observable } from 'rxjs'
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Developer } from '../Models/developer'
import {catchError} from 'rxjs/operators'
import {throwError} from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class DeveloperService {
  constructor(private http: HttpClient) {}

  getDevelopers(): Observable<any> {
    return this.http.get('https://releaseradarapi.herokuapp.com/api/developers')
  }
  getDeveloperById(developerId): Observable<any> {
    return this.http.get('https://releaseradarapi.herokuapp.com/api/developers/' + developerId)
  }
  delelteDeveloperById(developerId): Observable<any> {
    return this.http.delete('https://releaseradarapi.herokuapp.com/api/developers/' + developerId)
  }
  postDeveloper(developer: Developer): Observable<any> {
    return this.http.post<any>('https://releaseradarapi.herokuapp.com/api/developers/', developer)
      .pipe(catchError(this.errorHandler))
  }
  putDeveloper(developer: Developer, developerId): Observable<any> {
    return this.http.put<any>('https://releaseradarapi.herokuapp.com/api/developers/'+ developerId, developer)
      .pipe(catchError(this.errorHandler))
  }
  errorHandler(error: HttpErrorResponse){
    return throwError(error);
  }
}
