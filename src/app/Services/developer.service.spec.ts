import { TestBed, inject } from '@angular/core/testing';

import { DeveloperService } from './developer.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

describe('DeveloperService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule,RouterTestingModule],
  }));

  it('should be created', () => {
    const service: DeveloperService = TestBed.get(DeveloperService);
    expect(service).toBeTruthy();
  });

  it('should have delelteDeveloperById function',
  inject([DeveloperService], (service: DeveloperService) => {
    expect(service.delelteDeveloperById).toBeTruthy();
  }));
  
  it('should have getDeveloperById function',
  inject([DeveloperService], (service: DeveloperService) => {
    expect(service.getDeveloperById).toBeTruthy();
  }));
  
  it('should have getDevelopers function',
  inject([DeveloperService], (service: DeveloperService) => {
    expect(service.getDevelopers).toBeTruthy();
  }));
  
  it('should have postDeveloper function',
  inject([DeveloperService], (service: DeveloperService) => {
    expect(service.postDeveloper).toBeTruthy();
  }));
  
  it('should have putDeveloper function',
  inject([DeveloperService], (service: DeveloperService) => {
    expect(service.putDeveloper).toBeTruthy();
  }));
  
  it('should get users from API',
  inject([DeveloperService], (service: DeveloperService) => {
    expect(service.getDevelopers().subscribe(data => {
      expect(data.length).toBeGreaterThan(0);
    }));
  }));
});
