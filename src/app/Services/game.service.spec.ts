import { TestBed, inject } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { GameService } from './game.service'
import { HttpClientTestingModule } from '@angular/common/http/testing'

describe('GameService', () => {
  beforeEach(() => TestBed.configureTestingModule({imports: [HttpClientTestingModule,RouterTestingModule],}))

  it('should be created', () => {
    const service: GameService = TestBed.get(GameService)
    expect(service).toBeTruthy()
  })

  it('should have delelteGameById function',
  inject([GameService], (service: GameService) => {
    expect(service.delelteGameById).toBeTruthy();
  }));
  
  it('should have getGameById function',
  inject([GameService], (service: GameService) => {
    expect(service.getGameById).toBeTruthy();
  }));
  
  it('should have getGames function',
  inject([GameService], (service: GameService) => {
    expect(service.getGames).toBeTruthy();
  }));
  
  it('should have putGame function',
  inject([GameService], (service: GameService) => {
    expect(service.putGame).toBeTruthy();
  }));
  
  it('should have postGame function',
  inject([GameService], (service: GameService) => {
    expect(service.postGame).toBeTruthy();
  }));

  
  it('should get users from API',
  inject([GameService], (service: GameService) => {
    expect(service.getGames().subscribe(data => {
      expect(data.length).toBeGreaterThan(0);
    }));
  }));
})
