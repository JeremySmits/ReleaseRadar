import { TestBed, inject } from '@angular/core/testing';

import { PublisherService } from './publisher.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

describe('PublisherService', () => {
  beforeEach(() => TestBed.configureTestingModule({imports: [HttpClientTestingModule,RouterTestingModule],}));

  it('should be created', () => {
    const service: PublisherService = TestBed.get(PublisherService);
    expect(service).toBeTruthy();
  });

  it('should have postPublisher function',
  inject([PublisherService], (service: PublisherService) => {
    expect(service.postPublisher).toBeTruthy();
  }));
  
  it('should have getPublisherById function',
  inject([PublisherService], (service: PublisherService) => {
    expect(service.getPublisherById).toBeTruthy();
  }));
  
  it('should have getPublishers function',
  inject([PublisherService], (service: PublisherService) => {
    expect(service.getPublishers).toBeTruthy();
  }));
  
  it('should have putPublisher function',
  inject([PublisherService], (service: PublisherService) => {
    expect(service.putPublisher).toBeTruthy();
  }));
  
  it('should have deleltePublisherById function',
  inject([PublisherService], (service: PublisherService) => {
    expect(service.deleltePublisherById).toBeTruthy();
  }));

  
  it('should get users from API',
  inject([PublisherService], (service: PublisherService) => {
    expect(service.getPublishers().subscribe(data => {
      expect(data.length).toBeGreaterThan(0);
    }));
  }));
});
