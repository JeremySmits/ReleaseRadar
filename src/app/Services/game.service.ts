import { Injectable } from '@angular/core'
import { of, Observable } from 'rxjs'
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Game } from '../Models/game'
import {catchError} from 'rxjs/operators'
import {throwError} from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class GameService {
  constructor(private http: HttpClient) {}

  getGames(): Observable<any> {
    return this.http.get('https://releaseradarapi.herokuapp.com/api/games')
  }
  getGameById(gameId): Observable<any> {
    return this.http.get('https://releaseradarapi.herokuapp.com/api/games/' + gameId)
  }
  delelteGameById(gameId): Observable<any> {
    return this.http.delete('https://releaseradarapi.herokuapp.com/api/games/' + gameId)
  }
  postGame(game: Game): Observable<any> {
    return this.http.post<any>('https://releaseradarapi.herokuapp.com/api/games/', game)
      .pipe(catchError(this.errorHandler))
  }
  putGame(game: Game, gameId): Observable<any> {
    return this.http.put<any>('https://releaseradarapi.herokuapp.com/api/games/'+ gameId, game)
      .pipe(catchError(this.errorHandler))
  }
  errorHandler(error: HttpErrorResponse){
    return throwError(error);
  }
}
