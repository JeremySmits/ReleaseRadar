import { Injectable } from '@angular/core'
import { of, Observable } from 'rxjs'
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { User } from '../Models/user'
import {catchError} from 'rxjs/operators'
import {throwError} from 'rxjs'
import { Router } from '@angular/router'

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient, private router:Router) {}

  getUsers(): Observable<any> {
    return this.http.get('https://releaseradarapi.herokuapp.com/api/users')
  }
  postUser(user: User): Observable<any> {
    return this.http.post<any>('https://releaseradarapi.herokuapp.com/api/users/', user)
      .pipe(catchError(this.errorHandler))
  }
  loginUser(user: User): Observable<any> {
    return this.http.post<any>('https://releaseradarapi.herokuapp.com/api/Login', user)
      .pipe(catchError(this.errorHandler))
  }
  loggedIn()
  {
    return !!localStorage.getItem('token')
  }
  logoutUser()
  {
    localStorage.removeItem('token')
    this.router.navigate(['/games'])
  }
  getToken()
  {
    return localStorage.getItem('token')
  }
  errorHandler(error: HttpErrorResponse){
    return throwError(error);
  }
}
