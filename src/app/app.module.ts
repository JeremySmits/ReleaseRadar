import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http'
import { GameService } from './Services/game.service'
import {FormsModule} from '@angular/forms'
import {MatDialogModule} from '@angular/material/dialog'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './core/app/app.component'
import { UsecasesComponent } from './about/usecases/usecases.component'
import { NavbarComponent } from './core/navbar/navbar.component'
import { UsecaseComponent } from './about/usecases/usecase/usecase.component'
import { RouterModule } from '@angular/router'
import { DeveloperComponent } from './developer/developer/developer.component'
import { PublisherComponent } from './publisher/publisher/publisher.component'
import { DetailedgameComponent } from './game/detailedgame/detailedgame.component'
import { DetailedpublisherComponent } from './publisher/detailedpublisher/detailedpublisher.component'
import { DetaileddeveloperComponent } from './developer/detaileddeveloper/detaileddeveloper.component'
import { UpdategameComponent } from './game/updategame/updategame.component'
import { UpdatedeveloperComponent } from './developer/updatedeveloper/updatedeveloper.component'
import { UpdatepublisherComponent } from './publisher/updatepublisher/updatepublisher.component'
import { CreategameComponent } from './game/creategame/creategame.component'
import { CreatedeveloperComponent } from './developer/createdeveloper/createdeveloper.component'
import { CreatepublisherComponent } from './publisher/createpublisher/createpublisher.component';
import { DeletedialogComponent } from './core/deletedialog/deletedialog.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegisterComponent } from './core/register/register.component';
import { LoginComponent } from './core/login/login.component'
import { AuthGuard } from './Services/auth.guard'
import { TokenInterceptorService } from './Services/token-interceptor.service'
import { DevDashboardComponent } from './developer/devdashboard/devdashboard.component';
import { GameDashboardComponent } from './game/gamedashboard/gamedashboard.component';
import { PubDashboardComponent } from './publisher/pub-dashboard/pub-dashboard.component';
import { GameComponent } from './game/game/game.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    UsecasesComponent,
    UsecaseComponent,
    DeveloperComponent,
    PublisherComponent,
    DetailedgameComponent,
    DevDashboardComponent,
    DetailedpublisherComponent,
    DetaileddeveloperComponent,
    UpdategameComponent,
    UpdatedeveloperComponent,
    UpdatepublisherComponent,
    CreategameComponent,
    CreatedeveloperComponent,
    CreatepublisherComponent,
    DeletedialogComponent,
    RegisterComponent,
    LoginComponent,
    GameDashboardComponent,
    PubDashboardComponent,
    GameComponent
  ],
  entryComponents:[DeletedialogComponent],
  imports: [BrowserModule, HttpClientModule, RouterModule, NgbModule, AppRoutingModule,FormsModule, MatDialogModule, BrowserAnimationsModule ],
  providers: [GameService, AuthGuard,{
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptorService,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule {}
