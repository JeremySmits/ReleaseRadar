import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { UpdategameComponent } from './updategame.component'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { FormsModule } from '@angular/forms'
import { of } from 'rxjs'

describe('UpdategameComponent', () => {
  let component: UpdategameComponent
  let fixture: ComponentFixture<UpdategameComponent>
  let testDate = new Date(Date.now());
  let testGame = { "platform": "PC", "categorie": "Action", "name": "TestGame", "release": testDate, "price": 60, "image": "none",
   "description": "TestDescription", "trailer": "none", "publisher": "Test", "developer": "test", "user": "5e88804930828e0004c65ac0", "__v": 0 }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule,HttpClientTestingModule,FormsModule],
      declarations: [UpdategameComponent]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdategameComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should check if owner of game in storage', () => {
    component.gameModel.user = "testuser";
    spyOn(localStorage, "getItem").and.returnValue("testuser");
    spyOn(component.gameService, "getGameById").and.returnValue(of(testGame));
    component.loadGame();
    expect(component.owner).toBeTruthy;
  })

  it('should check if owner of game in storage', () => {
    component.gameModel.user = "testuser";
    spyOn(localStorage, "getItem").and.returnValue("Wrong user");
    spyOn(component.gameService, "getGameById").and.returnValue(of(testGame));
    component.loadGame();
    expect(component.owner).toBeFalsy;
  })

  it("should call gameservice.getGameById on loadGame", function() {
    spyOn(component.gameService, 'getGameById');
    component.loadGame();
    expect(component.gameService.getGameById).toHaveBeenCalled();
  });

  it("should call gameservice.putGame on submit", function() {
    spyOn(component.gameService, 'putGame').and.returnValue(of(testGame));
    component.onSubmit();
    expect(component.gameService.putGame).toHaveBeenCalled();
  });

  it('should load gamemodel in storage', () => {
    component.gameId = "1"
    spyOn(component.gameService, "getGameById").and.returnValue(of(testGame))
    component.loadGame()
    expect(component.gameModel).toEqual(testGame)
  })
})

