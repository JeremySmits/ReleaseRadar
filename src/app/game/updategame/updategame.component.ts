import { Component, OnInit } from '@angular/core'
import { Game } from '../../Models/game'
import {GameService} from '../../Services/game.service'
import { ActivatedRoute } from '@angular/router'
import { Router} from '@angular/router'

@Component({
  selector: 'app-updategame',
  templateUrl: './updategame.component.html',
  styleUrls: ['./updategame.component.scss']
})
export class UpdategameComponent implements OnInit {
  gameModel = new Game('',new Date,0,'','','','','','','',localStorage.getItem('userId'));
  errorMsg ='';
  gameId = this.route.snapshot.paramMap.get('id')
  owner = true;

  constructor(public gameService: GameService, private route: ActivatedRoute, private router: Router) {}
  ngOnInit( ) { this.loadGame()}
  onSubmit() {
    this.gameService.putGame(this.gameModel, this.gameId )
    .subscribe(
      data => this.routeToGame(),
      error => this.errorMsg = error.statusText
    )
  }
  routeToGame()
  {
    this.router.navigate(['../games'])
  }
  loadGame() {
    try
    {
      this.gameService.getGameById(this.gameId).subscribe(data => {this.gameModel = data
        if(this.gameModel.user != localStorage.getItem('userId'))
        {
          this.routeToGame();
          this.owner = false
        }
        })
      }
      catch
      {
        this.owner = false
      }
  }
    
}
