import { Component, OnInit } from '@angular/core'
import { GameService } from '../../Services/game.service'
import { UserService } from 'src/app/Services/user.service'

@Component({
  selector: 'app-gamedashboard',
  templateUrl: './gamedashboard.component.html',
  styleUrls: ['./gamedashboard.component.scss']
})
export class GameDashboardComponent implements OnInit {
  games = []
  constructor(private gameService: GameService,public userService: UserService) {}

  ngOnInit() {
    this.loadGames()
  }

  loadGames() {
    this.gameService.getGames().subscribe(data => (this.games = data))
  }
}
