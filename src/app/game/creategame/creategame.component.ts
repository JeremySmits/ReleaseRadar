import { Component, OnInit } from '@angular/core'
import { Game } from '../../Models/game'
import {GameService} from '../../Services/game.service'
import { Publisher } from '../../Models/publisher'
import {PublisherService} from '../../Services/publisher.service'
import { Developer } from '../../Models/developer'
import {DeveloperService} from '../../Services/developer.service'

@Component({
  selector: 'app-creategame',
  templateUrl: './creategame.component.html',
  styleUrls: ['./creategame.component.scss']
})
export class CreategameComponent implements OnInit {
  gameModel = new Game('',new Date,0,'','','','','','','',localStorage.getItem('userId'));
  errorMsg ='';
  publishers;
  developers;
  constructor(private gameService: GameService,private publisherService: PublisherService,private developerService: DeveloperService) {}
  ngOnInit() {
    this.loadPublishers();
    this.loadDevelopers();
  }
  onSubmit() {
    this.gameService.postGame(this.gameModel)
    .subscribe(
      data => window.location.href = '../games',
      error => this.errorMsg = error.statusText
    )
  }
  loadPublishers() {
    this.publisherService.getPublishers().subscribe(data => (this.publishers = data))
  }
  loadDevelopers() {
    this.developerService.getDevelopers().subscribe(data => (this.developers = data))
  }
}
