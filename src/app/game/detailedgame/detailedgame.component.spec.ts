import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { DetailedgameComponent } from './detailedgame.component'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { MatDialogModule } from '@angular/material'
import { of} from 'rxjs'
import { By } from '@angular/platform-browser'
import { DebugElement } from '@angular/core'

describe('DetailedgameComponent', () => {
  let component: DetailedgameComponent
  let fixture: ComponentFixture<DetailedgameComponent>
  let submitEl: DebugElement
  let testDate = new Date(Date.now());
  let testGame = { "platform": "PC", "categorie": "Action", "name": "TestGame", "release": testDate, "price": 60, "image": "none",
  "description": "TestDescription", "trailer": "none", "publisher": "Test", "developer": "test", "user": "5e88804930828e0004c65ac0", "__v": 0 }



  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule,HttpClientTestingModule, MatDialogModule],
      declarations: [DetailedgameComponent]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailedgameComponent)
    component = fixture.componentInstance
    submitEl = fixture.debugElement.query(By.css('button'));
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
  it('should load game in storage', () => {
    setGame()
    expect(component.game).toEqual(testGame)
  })

  it('should check owner in storage', () => {
    spyOn(component.authGaurd, "getLocalStorage").and.returnValue("5e88804930828e0004c65ac0")
    setGame()
    expect(component.owner).toBeTruthy()
  })

  it('should check wrong owner in storage', () => {
    spyOn(component.authGaurd, "getLocalStorage").and.returnValue("wrongtestuser")
    setGame()
    expect(component.owner).toBeFalsy()
  })

  it('should sanitize trailer', () => {
    expect(component.getEmbedUrl("https://www.youtube.com/embed/dQw4w9WgXcQ")).toBeTruthy();
  })

it('Setting enabled to false disabled the submit button', () => {
  component.owner = false;
  fixture.detectChanges();
  expect(submitEl.nativeElement.disabled).toBeTruthy();
});

it('Setting enabled to true enables the submit button', () => {
  component.owner = true;
  fixture.detectChanges();
  expect(submitEl.nativeElement.disabled).toBeFalsy();
});

function setGame()
{
  spyOn(component.gameService, "getGameById").and.returnValue(of(testGame))
  component.loadGame();
}
})
