import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { GameService } from '../../Services/game.service'
import { DomSanitizer } from '@angular/platform-browser'
import { MatDialog} from '@angular/material'
import { DeletedialogComponent } from '../../core/deletedialog/deletedialog.component'
import {Router} from '@angular/router'
import { UserService } from '../../Services/user.service'
import { AuthGuard } from 'src/app/Services/auth.guard'

@Component({
  selector: 'app-detailedgame',
  templateUrl: './detailedgame.component.html',
  styleUrls: ['./detailedgame.component.scss']
})
export class DetailedgameComponent implements OnInit {
    game = null
    owner = false;
    enabled = true;
    constructor(
    private route: ActivatedRoute,
    public gameService: GameService,
    private sanitizer: DomSanitizer,
    private dialog: MatDialog,
    private router: Router,
    public authGaurd: AuthGuard,
    public userService: UserService
  ) {}

  ngOnInit() {
    this.loadGame()
  }
  loadGame() {
    const gameId = this.route.snapshot.paramMap.get('id')
    console.log(gameId)
    this.gameService.getGameById(gameId).subscribe(data => {this.game = data
      if(this.game.user === this.authGaurd.getLocalStorage())
      {
        this.owner = true;
      }
    })
  }
  getEmbedUrl(trailer) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(trailer)
  }
  openDeleteDialog()
  {
    let dialogRef = this.dialog.open(DeletedialogComponent);
    dialogRef.afterClosed().subscribe(result => {
        if(result == "true")
        {
          const developerId = this.route.snapshot.paramMap.get('id')
          this.gameService.delelteGameById(developerId )
          .subscribe(
            data => this.router.navigate(['../games']),
            error => console.log(error.statusText)
          )
        }
    });
  }
}
