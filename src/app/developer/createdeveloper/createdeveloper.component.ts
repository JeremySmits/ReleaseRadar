import { Component, OnInit } from '@angular/core'
import { Developer } from '../../Models/developer'
import {DeveloperService} from '../../Services/developer.service'

@Component({
  selector: 'app-createdeveloper',
  templateUrl: './createdeveloper.component.html',
  styleUrls: ['./createdeveloper.component.scss']
})
export class CreatedeveloperComponent implements OnInit {
  developerModel = new Developer('','','','','','',localStorage.getItem('userId'));
  errorMsg ='';
  constructor(private developerService: DeveloperService) {}
  ngOnInit() {}
  onSubmit() {
    this.developerService.postDeveloper(this.developerModel)
    .subscribe(
      data => window.location.href = '../developers'
    )
  }
}
