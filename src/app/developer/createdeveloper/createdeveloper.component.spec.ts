import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { CreatedeveloperComponent } from './createdeveloper.component'
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormsModule }   from '@angular/forms';


describe('CreatedeveloperComponent', () => {
  let component: CreatedeveloperComponent
  let fixture: ComponentFixture<CreatedeveloperComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule,HttpClientTestingModule,FormsModule],
      declarations: [CreatedeveloperComponent]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatedeveloperComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
