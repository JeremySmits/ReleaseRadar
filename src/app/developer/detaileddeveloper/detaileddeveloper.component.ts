import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { DeveloperService } from '../../Services/developer.service'
import { DomSanitizer } from '@angular/platform-browser'
import { MatDialog} from '@angular/material'
import { DeletedialogComponent } from '../../core/deletedialog/deletedialog.component'
import {Router} from '@angular/router'
import { UserService } from '../../Services/user.service'

@Component({
  selector: 'app-detaileddeveloper',
  templateUrl: './detaileddeveloper.component.html',
  styleUrls: ['./detaileddeveloper.component.scss']
})
export class DetaileddeveloperComponent implements OnInit {
  developer = null
  owner = false;
  constructor(
    private route: ActivatedRoute,
    private developerService: DeveloperService,
    private sanitizer: DomSanitizer,
    private dialog: MatDialog,
    private router: Router,
    public userService: UserService
  ) {}

  ngOnInit() {
    this.loadDeveloper()
  }
  loadDeveloper() {
    const developerId = this.route.snapshot.paramMap.get('id')
    this.developerService.getDeveloperById(developerId).subscribe(data => {this.developer = data
      if(this.developer.user === localStorage.getItem('userId'))
      {
        this.owner = true;
      }
    })
  } 
  openDeleteDialog()
  {
    let dialogRef = this.dialog.open(DeletedialogComponent);
    dialogRef.afterClosed().subscribe(result => {
        if(result == "true")
        {
          const developerId = this.route.snapshot.paramMap.get('id')
          this.developerService.delelteDeveloperById(developerId )
          .subscribe(
            data => this.router.navigate(['../developers']),
            error => console.log(error.statusText)
          )
        }
    });
  }
}
