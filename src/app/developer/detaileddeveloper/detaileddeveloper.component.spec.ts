import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { DetaileddeveloperComponent } from './detaileddeveloper.component'
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatDialogModule} from '@angular/material'

describe('DetaileddeveloperComponent', () => {
  let component: DetaileddeveloperComponent
  let fixture: ComponentFixture<DetaileddeveloperComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule,HttpClientTestingModule, MatDialogModule],
      declarations: [DetaileddeveloperComponent ]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(DetaileddeveloperComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
