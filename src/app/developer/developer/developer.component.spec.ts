import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { DeveloperComponent } from './developer.component'
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('DeveloperComponent', () => {
  let component: DeveloperComponent
  let fixture: ComponentFixture<DeveloperComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule,HttpClientTestingModule],
      declarations: [DeveloperComponent]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(DeveloperComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
