import { Component, OnInit } from '@angular/core'
import { Developer } from '../../Models/developer'
import {DeveloperService} from '../../Services/developer.service'
import { ActivatedRoute } from '@angular/router'
import { Router} from '@angular/router'

@Component({
  selector: 'app-updatedeveloper',
  templateUrl: './updatedeveloper.component.html',
  styleUrls: ['./updatedeveloper.component.scss']
})
export class UpdatedeveloperComponent implements OnInit {
  developerModel = new Developer('','','','','','',localStorage.getItem('userId'));
  errorMsg ='';
  developerId = this.route.snapshot.paramMap.get('id')

  constructor(private developerService: DeveloperService, private route: ActivatedRoute, private router: Router) {}
  ngOnInit( ) { this.loadDeveloper()}
  onSubmit() {
    this.developerService.putDeveloper(this.developerModel, this.developerId )
    .subscribe(
      data => this.router.navigate(['../developers']),
      error => this.errorMsg = error.statusText
    )
  }
  loadDeveloper() {
    this.developerService.getDeveloperById(this.developerId).subscribe(data => {this.developerModel = data
      if(this.developerModel.user != localStorage.getItem('userId'))
      {
        this.router.navigate(['../dashboard'])
      }
    })
  }
}
