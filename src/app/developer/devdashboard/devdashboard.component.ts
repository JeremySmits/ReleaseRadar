import { Component, OnInit } from '@angular/core';
import { DeveloperService } from '../../Services/developer.service'
import { UserService } from 'src/app/Services/user.service'


@Component({
  selector: 'app-devdashboard',
  templateUrl: './devdashboard.component.html',
  styleUrls: ['./devdashboard.component.scss']
})
export class DevDashboardComponent implements OnInit {
  developers = []
  constructor(private developerService: DeveloperService,public userService: UserService) {}

  ngOnInit() {
    this.loadDevelopers()
  }

  loadDevelopers() {
    this.developerService.getDevelopers().subscribe(data => (this.developers = data))
  }
}
