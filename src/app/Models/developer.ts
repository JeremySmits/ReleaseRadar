export class Developer {
    constructor(
        public name: String,
        public description: String,
        public image: String,
        public website: String,
        public founded: String,
        public headquarter: String,
        public user:String
    ){}
}
