export class Game {
    constructor(
        public name: String,
        public release: Date,
        public price: Number,
        public image: String,
        public description: String,
        public trailer: String,
        public platform: String,
        public categorie: String,
        public publisher: String,
        public developer: String,
        public user:String
    ){}
}

