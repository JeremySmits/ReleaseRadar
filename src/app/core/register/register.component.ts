import { Component, OnInit } from '@angular/core'
import { User } from '../../Models/user'
import {UserService} from '../../Services/user.service'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  userModel = new User('','', false);
  errorMsg ='';
  constructor(public userService: UserService) {}
  ngOnInit() {}
  onSubmit() {
    this.userService.postUser(this.userModel)
    .subscribe(
      data => {
        localStorage.setItem('token', data.token)
        localStorage.setItem('userId', data.userId)
        window.location.href = '../dashboard'},
      error => this.errorMsg = 'User already exists, try a different username'
    )
  }
}