import { Component, OnInit } from '@angular/core'
import { User } from '../../Models/user'
import {UserService} from '../../Services/user.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  userModel = new User('','', false);
  errorMsg ='';
  constructor(public userService: UserService) {}
  ngOnInit() {}
  onSubmit() {
    this.userService.loginUser(this.userModel)
    .subscribe(
      data => {
        if(data != null )
        {
        localStorage.setItem('token', data.token)
        localStorage.setItem('userId', data.userId)
        window.location.href = '../dashboard'}
        else{
          this.errorMsg = 'Please use a valid username and password'
        }},
      error => this.errorMsg = error.statusText
    )
  }
}