import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { UsecasesComponent } from './about/usecases/usecases.component'

import { DevDashboardComponent } from './developer/devdashboard/devdashboard.component'

import { DeveloperComponent } from './developer/developer/developer.component'
import { PublisherComponent } from './publisher/publisher/publisher.component'

import { DetailedgameComponent } from './game/detailedgame/detailedgame.component'
import { DetaileddeveloperComponent } from './developer/detaileddeveloper/detaileddeveloper.component'
import { DetailedpublisherComponent } from './publisher/detailedpublisher/detailedpublisher.component'

import { UpdategameComponent } from './game/updategame/updategame.component'
import { UpdatedeveloperComponent } from './developer/updatedeveloper/updatedeveloper.component'
import { UpdatepublisherComponent } from './publisher/updatepublisher/updatepublisher.component'

import { CreategameComponent } from './game/creategame/creategame.component'
import { CreatedeveloperComponent } from './developer/createdeveloper/createdeveloper.component'
import { CreatepublisherComponent } from './publisher/createpublisher/createpublisher.component'

import { LoginComponent } from './core/login/login.component'
import { RegisterComponent } from './core/register/register.component'
import { AuthGuard } from './Services/auth.guard'
import { GameDashboardComponent } from './game/gamedashboard/gamedashboard.component'
import { PubDashboardComponent } from './publisher/pub-dashboard/pub-dashboard.component'
import { GameComponent } from './game/game/game.component'

const routes: Routes = [
  { path: '', redirectTo: 'games', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'about', component: UsecasesComponent },

  { path: 'games', component: GameComponent, children: [
    { path: '', component: GameDashboardComponent },
    { path: 'create', component: CreategameComponent ,canActivate:[AuthGuard]},
    { path: ':id', component: DetailedgameComponent },
    { path: 'edit/:id', component: UpdategameComponent ,canActivate:[AuthGuard]}
  ]},

  { path: 'developers', component: DeveloperComponent, children: [
    { path: '', component: DevDashboardComponent },
    { path: 'create', component: CreatedeveloperComponent ,canActivate:[AuthGuard]},
    { path: ':id', component: DetaileddeveloperComponent },
    { path: 'edit/:id', component: UpdatedeveloperComponent ,canActivate:[AuthGuard]}
  ]},

  { path: 'publishers', component: PublisherComponent, children: [
    { path: '', component: PubDashboardComponent },
    { path: 'create', component: CreatepublisherComponent ,canActivate:[AuthGuard]},
    { path: ':id', component: DetailedpublisherComponent },
    { path: 'edit/:id', component: UpdatepublisherComponent ,canActivate:[AuthGuard]}
  ]},

  { path: '**', redirectTo: '/games' }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
